﻿using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System;

namespace ClassLibrary
{
    class CryptoWorker
    {
        #region DllImport
        [DllImport("CppDll.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr EnCrypt(int[] a, long Len);
        [DllImport("CppDll.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr DeCrypt(int[] a, long Len);
        [DllImport("CppDll.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr FastEnCrypt(int[] a, long Len);
        [DllImport("CppDll.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr FastDeCrypt(int[] a, long Len);
        #endregion
        #region Secure
        public static int[] EncryptArray(int[] Array)
        {
            IntPtr Ptr = EnCrypt(Array, Array.Length);
            int[] result = new int[Array.Length * 2];
            Marshal.Copy(Ptr, result, 0, Array.Length * 2);
            return result;
        }
        public static byte[] EncryptArray(byte[] Array) => EncryptArray(Array.Select(b => (int)b).ToArray()).Select(b => (byte)b).ToArray();
        public static List<int> EncryptArray(List<int> Array) => EncryptArray(Array.ToArray()).ToList();
        public static List<byte> EncryptArray(List<byte> Array) => EncryptArray(Array.Select(b => (int)b).ToArray()).Select(b => (byte)b).ToList();
        public static int[] DeCryptArray(int[] Array)
        {
            IntPtr Ptr = DeCrypt(Array, Array.Length);
            int[] result = new int[Array.Length / 2];
            Marshal.Copy(Ptr, result, 0, Array.Length / 2);
            return result;
        }
        public static byte[] DeCryptArray(byte[] Array) => DeCryptArray(Array.Select(b => (int)b).ToArray()).Select(b => (byte)b).ToArray();
        public static List<int> DeCryptArray(List<int> Array) => DeCryptArray(Array.ToArray()).ToList();
        public static List<byte> DeCryptArray(List<byte> Array) => DeCryptArray(Array.Select(b => (int)b).ToArray()).Select(b => (byte)b).ToList();
        #endregion
        #region Fast
        public static int[] EncryptArrayFast(int[] Array)
        {
            IntPtr Ptr = FastEnCrypt(Array, Array.Length);
            int[] result = new int[Array.Length];
            Marshal.Copy(Ptr, result, 0, Array.Length);
            return result;
        }
        public static byte[] EncryptArrayFast(byte[] Array) => EncryptArrayFast(Array.Select(b => (int)b).ToArray()).Select(b => (byte)b).ToArray();
        public static List<int> EncryptArrayFast(List<int> Array) => EncryptArrayFast(Array.ToArray()).ToList();
        public static List<byte> EncryptArrayFast(List<byte> Array) => EncryptArrayFast(Array.Select(b => (int)b).ToArray()).Select(b => (byte)b).ToList();
        public static int[] DeCryptArrayFast(int[] Array)
        {
            IntPtr Ptr = FastDeCrypt(Array, Array.Length);
            int[] result = new int[Array.Length];
            Marshal.Copy(Ptr, result, 0, Array.Length);
            return result;
        }
        public static byte[] DeCryptArrayFast(byte[] Array) => DeCryptArrayFast(Array.Select(b => (int)b).ToArray()).Select(b => (byte)b).ToArray();
        public static List<int> DeCryptArrayFast(List<int> Array) => DeCryptArrayFast(Array.ToArray()).ToList();
        public static List<byte> DeCryptArrayFast(List<byte> Array) => DeCryptArrayFast(Array.Select(b => (int)b).ToArray()).Select(b => (byte)b).ToList();
        #endregion
    }
}
