﻿using System;

namespace ClassLibrary
{
    public class DataBaseObject : IDisposable
    {
        public string UserName { get; set; }
        public string MachineName { get; set; }
        public string Date { get; set; }
        public byte[] Data { get; set; }
        public void Dispose()
        {
            Data = new byte[0];
            Data = null;
            MachineName = UserName = null;
        }
    }
}