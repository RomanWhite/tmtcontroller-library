﻿using System.Collections.Generic;
//using System.Data.SQLite;
using System.IO;
using System;

namespace ClassLibrary
{
    public class DataBaseWorker
    {
        const string DateFormat = "yyyy-MM-dd HH:mm:ss";
        const string ID_Field = "id";
        const string UserName_Field = "UserName";
        const string MachineName_Field = "MachineName";
        const string Date_Field = "Date";
        const string Data_Field = "Data";
        const string TableName = "Data";
        const string Limit = "LIMIT";
        string DBPath;
        public DataBaseWorker(string dbpath)
        {
            //DBPath = dbpath;
            //if (!File.Exists(DBPath))
            //    SQLiteConnection.CreateFile(DBPath);
            //using (SQLiteConnection Connect = new SQLiteConnection("Data Source=" + DBPath + "; Version=3;"))
            //{
            //    string commandText = "CREATE TABLE IF NOT EXISTS [" + TableName + "] ( [" + ID_Field + "] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [" + UserName_Field + "] TEXT, [" + MachineName_Field + "] TEXT, [" + Date_Field + "] TEXT, [" + Data_Field + "] BLOB)";
            //    SQLiteCommand Command = new SQLiteCommand(commandText, Connect);
            //    Connect.Open();
            //    Command.ExecuteNonQuery();
            //    Connect.Close();
            //}
        }
        public void AddData(string UserName, string MachineName, DateTime Date, byte[] DataArray)
        {
            //using (SQLiteConnection Connect = new SQLiteConnection("Data Source=" + DBPath + "; Version=3;"))
            //{
            //    string commandText = "INSERT INTO [" + TableName + "] ([" + UserName_Field + "], [" + MachineName_Field + "], [" + Date_Field + "], [" + Data_Field + "]) VALUES(@usernamevalue, @machinenamevalue, @datevalue, @datavalue)";
            //    SQLiteCommand Command = new SQLiteCommand(commandText, Connect);
            //    Command.Parameters.AddWithValue("@usernamevalue", UserName);
            //    Command.Parameters.AddWithValue("@machinenamevalue", MachineName);
            //    Command.Parameters.AddWithValue("@datevalue", Date.ToString("yyyy-MM-dd HH:mm:ss"));
            //    Command.Parameters.AddWithValue("@datavalue", DataArray);
            //    Connect.Open();
            //    Command.ExecuteNonQuery();
            //    Connect.Close();
            //}
        }
        public List<DataBaseObject> ReadAllData(int MaxCount)
        {
            //using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + DBPath + "; Version=3;"))
            //{
            //    Connect.Open();
            //    SQLiteCommand Command = new SQLiteCommand
            //    {
            //        Connection = Connect,
            //        CommandText = @"SELECT * FROM [" + TableName + "]"
            //    };
            //    SQLiteDataReader sqlReader = Command.ExecuteReader();
                List<DataBaseObject> Result = new List<DataBaseObject>();
            //    while (sqlReader.Read())
            //        Result.Add(new DataBaseObject()
            //        {
            //            UserName = (string)sqlReader[UserName_Field],
            //            MachineName = (string)sqlReader[MachineName_Field],
            //            Date = (string)sqlReader[Date_Field],
            //            Data = (byte[])sqlReader[Data_Field]
            //        });
            //    Connect.Close();
                return Result;
            //}
        }
        public List<DataBaseObject> ReadAllData(string UserName, string UachineName, DateTime StartDate, DateTime EndDate, int MaxCount)
        {
            //using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + DBPath + "; Version=3;"))
            //{
            //    Connect.Open();
            //    SQLiteCommand Command = new SQLiteCommand
            //    {
            //        Connection = Connect,
            //        CommandText = @"SELECT * FROM [" + TableName + "] WHERE " +
            //        (string.IsNullOrEmpty(UserName) ? "" : (UserName_Field + " == \"" + UserName + "\" AND ")) +
            //        (string.IsNullOrEmpty(UachineName) ? "" : (MachineName_Field + " == \"" + UachineName + "\" AND ")) +
            //        Date_Field + " > \"" + StartDate.ToString("yyyy-MM-dd HH:mm:ss") + "\" AND " +
            //        Date_Field + " < \"" + EndDate.ToString("yyyy-MM-dd HH:mm:ss") + "\"" +
            //        Limit + " \"" + MaxCount.ToString() + "\""
            //    };
                List<DataBaseObject> Result = new List<DataBaseObject>();
            //    using (SQLiteDataReader sqlReader = Command.ExecuteReader())
            //    {
            //        while (sqlReader.Read())
            //            Result.Add(new DataBaseObject()
            //            {
            //                UserName = (string)sqlReader[UserName_Field],
            //                MachineName = (string)sqlReader[MachineName_Field],
            //                Date = (string)sqlReader[Date_Field],
            //                Data = (byte[])sqlReader[Data_Field]
            //            });
            //    }
            //    Connect.Close();
                return Result;
            //}
        }
    }
}
