﻿using System.Collections.Generic;
using ClassLibrary.TMTService;

namespace ClassLibrary
{
    public class DownloadedData
    {
        public delegate void VoidHandler();
        public event VoidHandler OnStop;
        public event VoidHandler OnUpdated;
        public bool IsLoadStopped { get; private set; } = false;
        List<ScreenShotData> data = new List<ScreenShotData>();
        object Locker = new object();
        public List<ScreenShotData> Data
        {
            get
            {
                lock (Locker)
                {
                    return data;
                }
            }
        }
        public void AddData(ScreenShotData[] D)
        {
            lock (Locker)
            {
                data.AddRange(D);
                OnUpdated?.Invoke();
            }
        }
        public void AddData(List<ScreenShotData> D)
        {
            lock (Locker)
            {
                data.AddRange(D);
                OnUpdated?.Invoke();
            }
        }
        public void Stop()
        {
            IsLoadStopped = true;
            OnStop?.Invoke();
        }
    }
}