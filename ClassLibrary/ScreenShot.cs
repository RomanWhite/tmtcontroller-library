﻿using System.Collections.Generic;
using ClassLibrary.TMTService;
using System.Linq;
using System.Text;
using System.IO;
using System;

namespace ClassLibrary
{
    public class ScreenShot
    {
        #region Variables
        public enum PhotoExtensionEnum { JPEG, BMP }
        public const string BMPExtention = ".bmp";
        public const string JPGExtention = ".jpg";
        public const string CryptoExtention = ".crypto";
        public string GetDirectoryPath(string path)
        {
            var directoryPath = path != null ? path : "C://TMTServiceScreenshots";
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            directoryPath += "//" + MachineName;
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            directoryPath += "//" + UserName;
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            directoryPath += "//" + Date.ToString("dd_MM_yyyy");
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);
            return directoryPath;
        }
        public DateTime Date
        {
            get;
            private set;
        }
        public bool Encrypted { get; set; }
        public byte[] ByteValue { get; set; }
        public string UName;
        public string UserName { get; set; }
        public string MachineName { get; set; }
        public string Key { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int ScreenNum { get; set; }
        public int Num { get; set; }
        #endregion
        #region ByteConstructor
        public ScreenShot(byte[] RawData)
        {
            int StartIndex = 0;
            int Length = 0;
            //ByteValue
            {
                int LengthLength = RawData[StartIndex++];
                Length = BitConverter.ToInt32(RawData.Skip(StartIndex).Take(LengthLength).ToArray(), 0);
                StartIndex += LengthLength;
                ByteValue = RawData.Skip(StartIndex).Take(Length).ToArray();
                StartIndex += Length;
            }
            //UserName
            {
                Length = RawData[StartIndex++];
                UserName = Encoding.UTF8.GetString(RawData, StartIndex, Length);
                StartIndex += Length;
            }
            //MachineName
            {
                Length = RawData[StartIndex++];
                MachineName = Encoding.UTF8.GetString(RawData, StartIndex, Length);
                StartIndex += Length;
            }
            //Encrypted
            {
                Length = RawData[StartIndex++];
                Encrypted = RawData[StartIndex++] == 1;
            }
            //ScreenNum
            {
                Length = RawData[StartIndex++];
                ScreenNum = RawData[StartIndex++];
            }
            //Num
            {
                Length = RawData[StartIndex++];
                Num = BitConverter.ToInt32(RawData.Skip(StartIndex).Take(Length).ToArray(), 0);
                StartIndex += Length;
            }
            //Num
            {
                Length = RawData[StartIndex++];
                X = BitConverter.ToInt32(RawData.Skip(StartIndex).Take(Length).ToArray(), 0);
                StartIndex += Length;
            }
            //Num
            {
                Length = RawData[StartIndex++];
                Y = BitConverter.ToInt32(RawData.Skip(StartIndex).Take(Length).ToArray(), 0);
                StartIndex += Length;
            }
            //Date
            {
                Length = RawData[StartIndex++];
                if (Length == 6)
                {
                    byte[] Raw = RawData.Skip(StartIndex).Take(Length).ToArray();
                    Date = new DateTime(Raw[0] + 2000, Raw[1], Raw[2], Raw[3], Raw[4], Raw[5]);
                }
            }
        }
        public byte[] ToByteArray()
        {
            List<byte> Result = new List<byte>();
            //ByteValue
            {
                byte[] TempLengthArray = BitConverter.GetBytes(ByteValue.Length);
                Result.Add((byte)TempLengthArray.Length);
                Result.AddRange(TempLengthArray);
                Result.AddRange(ByteValue);
            }
            //UserName
            {
                byte[] TempArray = Encoding.ASCII.GetBytes(UserName);
                Result.Add((byte)TempArray.Length);
                Result.AddRange(TempArray);
            }
            //MachineName
            {
                byte[] TempArray = Encoding.ASCII.GetBytes(MachineName);
                Result.Add((byte)TempArray.Length);
                Result.AddRange(TempArray);
            }
            //Encrypted
            {
                Result.Add(1);
                Result.Add((byte)(Encrypted ? 1 : 0));
            }
            //ScreenNum
            {
                Result.Add(1);
                Result.Add((byte)ScreenNum);
            }
            //Num
            {
                byte[] TempArray = BitConverter.GetBytes(Num);
                Result.Add((byte)TempArray.Length);
                Result.AddRange(TempArray);
            }
            //X
            {
                byte[] TempArray = BitConverter.GetBytes(X);
                Result.Add((byte)TempArray.Length);
                Result.AddRange(TempArray);
            }
            //Y
            {
                byte[] TempArray = BitConverter.GetBytes(Y);
                Result.Add((byte)TempArray.Length);
                Result.AddRange(TempArray);
            }
            //Date
            {
                Result.Add(6);
                Result.Add((byte)(Date.Year - 2000));
                Result.Add((byte)Date.Month);
                Result.Add((byte)Date.Day);
                Result.Add((byte)Date.Hour);
                Result.Add((byte)Date.Minute);
                Result.Add((byte)Date.Second);
            }
            return Result.ToArray();
        }
        public byte[] ToShortByteArray()
        {
            List<byte> Result = new List<byte>();
            //ByteValue
            {
                byte[] TempLengthArray = BitConverter.GetBytes(ByteValue.Length);
                Result.Add((byte)TempLengthArray.Length);
                Result.AddRange(TempLengthArray);
                Result.AddRange(ByteValue);
            }
            //Encrypted
            {
                Result.Add(1);
                Result.Add((byte)(Encrypted ? 1 : 0));
            }
            //ScreenNum
            {
                Result.Add(1);
                Result.Add((byte)ScreenNum);
            }
            //Num
            {
                byte[] TempArray = BitConverter.GetBytes(Num);
                Result.Add((byte)TempArray.Length);
                Result.AddRange(TempArray);
            }
            //X
            {
                byte[] TempArray = BitConverter.GetBytes(X);
                Result.Add((byte)TempArray.Length);
                Result.AddRange(TempArray);
            }
            //Y
            {
                byte[] TempArray = BitConverter.GetBytes(Y);
                Result.Add((byte)TempArray.Length);
                Result.AddRange(TempArray);
            }
            return Result.ToArray();
        }
        public ScreenShot(DataBaseObject DBObject)
        {
            UserName = DBObject.UserName;
            MachineName = DBObject.MachineName;
            string[] RawDate = DBObject.Date.Split(new char[] { '-', ':', ' ' });
            if (RawDate.Length == 6)
                Date = new DateTime(int.Parse(RawDate[0]), int.Parse(RawDate[1]), int.Parse(RawDate[2]), int.Parse(RawDate[3]), int.Parse(RawDate[4]), int.Parse(RawDate[5]));
            int StartIndex = 0;
            int Length = 0;
            //ByteValue
            {
                int LengthLength = DBObject.Data[StartIndex++];
                Length = BitConverter.ToInt32(DBObject.Data.Skip(StartIndex).Take(LengthLength).ToArray(), 0);
                StartIndex += LengthLength;
                ByteValue = DBObject.Data.Skip(StartIndex).Take(Length).ToArray();
                StartIndex += Length;
            }
            //Encrypted
            {
                Length = DBObject.Data[StartIndex++];
                Encrypted = DBObject.Data[StartIndex++] == 1;
            }
            //ScreenNum
            {
                Length = DBObject.Data[StartIndex++];
                ScreenNum = DBObject.Data[StartIndex++];
            }
            //Num
            {
                Length = DBObject.Data[StartIndex++];
                Num = BitConverter.ToInt32(DBObject.Data.Skip(StartIndex).Take(Length).ToArray(), 0);
                StartIndex += Length;
            }
            //X
            {
                Length = DBObject.Data[StartIndex++];
                X = BitConverter.ToInt32(DBObject.Data.Skip(StartIndex).Take(Length).ToArray(), 0);
                StartIndex += Length;
            }
            //Y
            {
                Length = DBObject.Data[StartIndex++];
                Y = BitConverter.ToInt32(DBObject.Data.Skip(StartIndex).Take(Length).ToArray(), 0);
            }
        }
        #endregion
        public ScreenShot(ScreenShotData Screen)
        {
            if(Screen.Value == null)
            {
                int a = 2;
            }

            ByteValue = Screen.Value;
            Key = Screen.Key;
            Date = Screen.Date;
            UserName = Screen.UserName;
            MachineName = Screen.MachineName;
            ScreenNum = Screen.ScreenNum;
            Encrypted = Screen.IsEnCrypted;
            X = Screen.X;
            Y = Screen.Y;
            Num = Screen.Num;
        }
        public void SaveAsJpg(string path = null)
        {
            SavePhoto(PhotoExtensionEnum.JPEG, path);
        }
        public void SaveAsBmp(string path = null)
        {
            SavePhoto(PhotoExtensionEnum.BMP, path);
        }
        public void SavePhoto(PhotoExtensionEnum photoExtension, string path)
        {
            var directoryPath = GetDirectoryPath(path);

            var filePath = $"{directoryPath}/{Date.ToString("HH_mm_ss")}_screen_{ScreenNum}";
            ;
            filePath += photoExtension == PhotoExtensionEnum.JPEG ? JPGExtention :
                photoExtension == PhotoExtensionEnum.BMP ? BMPExtention :
                "";

            if (ByteValue != null)
            {
                File.WriteAllBytes(filePath, ByteValue);
            }
        }

        public void Encrypt()
        {
            if (!Encrypted)
            {
                ByteValue = CryptoWorker.EncryptArrayFast(ByteValue);
                Encrypted = true;
            }
        }
        public void Decrypt()
        {
            if (Encrypted)
            {
                ByteValue = CryptoWorker.DeCryptArrayFast(ByteValue);
                Encrypted = false;
            }
        }
    }
}
