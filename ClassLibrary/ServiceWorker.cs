﻿using System.Collections.Generic;
using ClassLibrary.TMTService;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace ClassLibrary
{
    public class ServiceWorker : IDisposable
    {
        public delegate void LogHandler(string LogText);
        public static event LogHandler LogEvent;

        const string MachineName = "Library";
        const string UserName = "Controller";
        const int MaxCount = 50;

        TMTContractClient Contract;
        MachineInfo ThisMachineInfo = new MachineInfo() { MachineName = MachineName, UserName = UserName };
        public ServiceWorker()
        {
            Contract = WCF.Contract.GetContrcat();
            while (!Contract.Hello(ThisMachineInfo)) ;
        }
        public void Close()
        {
            if (WCF.Contract.GetContrcat().GoodBye(ThisMachineInfo))
                Console.WriteLine("GoodBye is ok");
            WCF.Contract.GetContrcat().Close();
        }
        public void Dispose()
        {
            Contract.Close();
        }
        /// <summary>
        /// Списко клиентов
        /// </summary>
        public List<ClientInfo> GetClients()
        {
            return Contract.LoadClientList().ToList();
        }
        /// <summary>
        /// Скриншоты клиента с ...... по ......
        /// </summary>
        public DownloadedData GetScreenShots(ClientInfo Info, DateTime StartDate, DateTime EndDate)
        {
            DownloadedData Result = new DownloadedData();
            Task.Run(() =>
            {
                DateTime TempStartDate = StartDate;
                while (TempStartDate <= EndDate)
                {
                    ScreenShotData[] Temp = Contract.GetDataForDate(Info, TempStartDate, EndDate, MaxCount);
                    ScreenShotData[] TempNull = Temp.Where(s => s.Value == null || s.Value.Length == 0).ToArray();
                    if(TempNull.Length > 0)
                        LogEvent?.Invoke("Loaded " + TempNull.Length + " null objects");
                    Temp = Temp.Where(s => s.Value != null && s.Value.Length > 0).ToArray();
                    TempStartDate = Temp.Select(s => s.Date).Max();
                    Result.AddData(Temp);
                }
            });
            return Result;
        }
        /// <summary>
        /// Скриншоты юзера с ...... по ......
        /// </summary>
        public DownloadedData GetScreenShotsByUser(string UserName, DateTime StartDate, DateTime EndDate)
        {
            DownloadedData Result = new DownloadedData();
            Task.Run(() =>
            {
                DateTime TempStartDate = StartDate;
                while (TempStartDate <= EndDate)
                {
                    if (Contract.State == System.ServiceModel.CommunicationState.Closed)
                        break;
                    try
                    {
                        ScreenShotData[] Temp = Contract.GetUserData(UserName, TempStartDate, EndDate, MaxCount);
                        ScreenShotData[] TempNull = Temp.Where(s => s.Value == null || s.Value.Length == 0).ToArray();
                        if (TempNull.Length > 0)
                            LogEvent?.Invoke("Loaded " + TempNull.Length + " null objects");
                        Temp = Temp.Where(s => s.Value != null && s.Value.Length > 0).ToArray();
                        if (Temp.Length == 0)
                            break;
                        TempStartDate = Temp.Select(s => s.Date).Max();
                        Result.AddData(Temp);
                    }
                    catch (Exception Ex)
                    {
                    }
                }
                Result.Stop();
            });
            return Result;
        }
        /// <summary>
        /// Скриншоты с устройства с ...... по ......
        /// </summary>
        public DownloadedData GetScreenShotsByMachine(string MachoneName, DateTime StartDate, DateTime EndDate)
        {
            DownloadedData Result = new DownloadedData();
            Task.Run(() =>
            {
                DateTime TempStartDate = StartDate;
                while (TempStartDate <= EndDate)
                {
                    ScreenShotData[] Temp = Contract.GetMachineData(MachoneName, TempStartDate, EndDate, MaxCount);
                    ScreenShotData[] TempNull = Temp.Where(s => s.Value == null || s.Value.Length == 0).ToArray();
                    if (TempNull.Length > 0)
                        LogEvent?.Invoke("Loaded " + TempNull.Length + " null objects");
                    Temp = Temp.Where(s => s.Value != null && s.Value.Length > 0).ToArray();
                    TempStartDate = Temp.Select(s => s.Date).Max();
                    Result.AddData(Temp);
                }
            });
            return Result;
        }
        /// <summary>
        /// Установка новых настроек клиенту
        /// </summary>
        public void SetSettings(Settings NewValue, ClientInfo Client)
        {
            Contract.SetSettings(NewValue, Client);
        }
        /// <summary>
        /// ТаймЛайн для скриншотов
        /// Инетрвал: длительность в минутах, доля активности относительно максимальной за дату
        /// </summary>
        public ScreenTimeLine GetScreenTimeLine(ClientInfo Info, DateTime date)
        {
            return Contract.GetScreenTimeLine(Info, date);
        }
    }
}