﻿using ClassLibrary.TMTService;
using System.ServiceModel;

namespace ClassLibrary.WCF
{
    class Contract
    {
        static InstanceContext CallBack = new InstanceContext(new TMTContractClientCallBack());
        static TMTContractClient client = new TMTContractClient(CallBack, "CustomBinding_ITMTContract");
        public static void TryNewConnect()
        {
            CallBack = new InstanceContext(new TMTContractClientCallBack());
            client = new TMTContractClient(CallBack, "CustomBinding_ITMTContract");
        }
        /// <summary>
        /// Получить ссылку на службу
        /// </summary>
        public static TMTContractClient GetContrcat()
        {
            try
            {
                if (client == null ||
                    client.State == CommunicationState.Closing ||
                    client.State == CommunicationState.Closed ||
                    client.State == CommunicationState.Faulted)
                    TryNewConnect();
                return client;
            }
            catch (System.Exception)
            {
                return null;
            }
        }
    }
}