﻿using System.Threading;
using ClassLibrary;
using System;
using System.Diagnostics;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //App.config нужно прописывать в клиенте

            var ts = new Stopwatch();
            ts.Start();
            ServiceWorker.LogEvent += (lt) => Console.WriteLine("Log: " + lt);

            using (ServiceWorker Worker = new ServiceWorker())
            {
                var Clients = Worker.GetClients();
                foreach (var Client in Clients)
                {
                    Console.WriteLine(Client.MachineName + "/" + Client.UserName);
                }

                DownloadedData D_Data = Worker.GetScreenShotsByUser(
                    Clients[0].UserName,
                    DateTime.Now.Date,
                    DateTime.Now);

                ////OLD
                //bool ok = false;
                //D_Data.OnStop += () => ok = true;
                //while (!ok)
                //    Thread.Sleep(100);

                //NEW
                D_Data.OnUpdated += () => Console.WriteLine(D_Data.Data.Count);
                while (!D_Data.IsLoadStopped)
                    Thread.Sleep(100);

                foreach (var item in D_Data.Data)
                {
                    ScreenShot Temp = new ScreenShot(item);
                    //Console.WriteLine($"Size:{(Temp.ByteValue == null ? "null" : Temp.ByteValue.Length.ToString())}");
                    if (Temp.Encrypted)
                        Temp.Decrypt();
                    Temp.SaveAsJpg("C://FFF");
                }
                Console.WriteLine("Loaded: " + D_Data.Data.Count);

                //Worker.Close();
                //Environment.Exit(0);
            }
            ts.Stop();
            Console.WriteLine(new TimeSpan(ts.ElapsedTicks));
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}